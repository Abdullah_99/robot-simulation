#ifndef HEADER01_H_INCLUDED
#define HEADER01_H_INCLUDED



#endif // HEADER01_H_INCLUDED
#define Motor11 0
#define Motor12 1
#define Motor21 2
#define Motor22 3

unsigned char forward(){
unsigned char robot_control=0;
//Motor1 cw
robot_control|=(1<<Motor11);
robot_control&=~(1<<Motor12);
//Motor2 cw
robot_control|=(1<<Motor21);
robot_control&=~(1<<Motor22);
return robot_control;
}
unsigned char backward(){
unsigned char robot_control=0;
//Motor1 ccw
robot_control|=(1<<Motor12);
robot_control&=~(1<<Motor11);
//Motor2 ccw
robot_control|=(1<<Motor22);
robot_control&=~(1<<Motor21);
return robot_control;
}
unsigned char right(){
unsigned char robot_control=0;
//Motor1 cw
robot_control|=(1<<Motor11);
return robot_control;
}
unsigned char left(){
unsigned char robot_control=0;
//Motor2 cw
robot_control|=(1<<Motor21);
return robot_control;
}
unsigned char stop(){
unsigned char robot_control=0;
return robot_control;
}
