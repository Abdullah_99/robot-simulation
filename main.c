#include <stdio.h>
#include <stdlib.h>
#include "header01.h"
int main()
{
    char dir ;
    unsigned char control_signal;

    printf("Enter w,a,s,d or x \n");
    for(;;){
    scanf("%s",&dir);
    if (dir=='w'){
            control_signal=forward();
        printf("Move robot forward,control=%x \n",control_signal);
    }
    else if (dir=='s'){
        control_signal=backward();
        printf("Move robot backward,control=%x \n",control_signal);
    }
    else if(dir=='d'){
        control_signal=right();
        printf("Move robot right,control=%x \n",control_signal);
    }
    else if(dir=='a'){
        control_signal=left();
        printf("Move robot left,control=%x \n",control_signal);
    }
    else if(dir=='x'){
        control_signal=stop();
        printf("robot stop,control=%x \n",control_signal);
    }
    }
}
